# External libraries

This chapter has been co-authored by my students as one of the assignments for the kickstart programming course. We have worked in small study groups, each group choosing one library to work on. The idea was that each group introduces the library, describing the basic steps and to think of a small exercise. The groups were formed around the libraries _video_, _fontastic_, _controlp5_ and _minim_.

  * Minim: Stéphanie, Géraldine, Claudia, Fabrice
  * Video: Tin, Lea, Lukas, Andrin
  * Controlp5: Simon, Frederik, Chhail   
  * Fontastic: Raphael, Seline, Vera

----
## Minim

### Was ist ‘minim’?

Minim ist eine Audiobibliothek, die die JavaSound API, Tritonus und Javazoom’s MP3SPI verwendet, um eine einfach zu bedienende Audiobibliothek für Entwickler in Processing bereitzustellen. Die Philosophie hinter der API ist es, die Integration von Audio in Ihrem Sketch so einfach wie möglich zu gestalten und gleichzeitig ein angemessenes Maß an Flexibilität für fortgeschrittene Benutzer zu bieten.


### Eigenschaften

* AudioPlayer: Mono- und Stereo-Wiedergabe von WAV-, AIFF-, AU-, SND- und MP3-Dateien.
* AudioMetaData: Ein Objekt, das mit Metadaten über eine Datei gefüllt ist, wie z.B. ID3-Tags.
* AudioRecorder: Mono- und Stereo-Audioaufnahme entweder gepuffert oder direkt auf Diskette.
* AudioInput: Mono- und Stereo-Eingangsmonitoring.
* AudioOutput: Mono- und Stereo-Klangsynthese.
* FFT: Führen Sie eine Fourier-Transformation auf Audiodaten durch, um ein Frequenzspektrum zu erzeugen.
* BeatDetect: eine Klasse für die Beat-Erkennung.

### Verwendung

Das Grundgerüst zum Abspielen von Audiodateien untergliedert sich in zwei Bereiche. Zum Ersten muss minim in den aktuellen Sketch einbezogen werden. Dies geschieht über die Zeile

```Java
import ddf.minim.*;
```

welche automatisch durch das Auswählen von “Sketch” → “Import Library” → “minim” erzeugt wird. Im globalen Bereich, zwischen import und setup(), erfolgt die Definition der Variablen für die minim- und die Player-Instanz (global da sie im setup() sowie im draw() benötigt werden). Zu Beginn des Processing Sketches wird die Datei mit Hilfe von minim aus dem ‘/data’ Ordner geladen und das Abspielen durch play() eingeleitet.

**data Ordner erstellen**

Erstelle im Ordner von Processing einen Ordner Namens “data”. Platziere dort deine Musikdateien.

Du kannst die Datei auch per drag and drop direkt ins Sketchfenster ziehen. Processing erstellt nun automatisch einen “data” Ordner.

**minim in den aktuellen Sketch einbeziehen**

```Java
    import ddf.minim.*;
```

**Objektvariable des Typs Minim erstellen**
```Java
    Minim minim;
```
**AudioPlayer definieren**
```Java
    AudioPlayer newplayer;
```

**Initalisiere minim**
```Java
    minim = new Minim(this);
```
**Audiodatei aus dem data-Ordner laden**
```Java
    newplayer = minim.loadFile("track.aif");
```
**Audiodatei abspielen**
```Java
     newplayer.play ();
```
**Beispiel zur Ansicht**
```Java
    // Einbeziehen des minim-Audio-Toolkits
    import ddf.minim.*;
    // Instanz der minim Bibliothek
    Minim minim;
    // Instanz die das geladene Audiodokument repraesentiert
    AudioPlayer newplayer;

    void setup () {
    // Audiotoolkit erstellen
    minim = new Minim (this);
    // Audiodatei aus dem data-Ordner laden
    newplayer = minim.loadFile ("track.aif");
    // Audiodatei abspielen
    newplayer.play ();
    }

    void draw () {
    // Moegliches Auswerden des aktuellen Spektrums
    // bzw. von anderen Audioeigenschaften.
    }
```

**Weitere Befehle**

pause() pausiert den Abspielvorgang.

play() startet den Abspielvorgang an der aktuellen Position.

play(int millis) startet den Abspielvorgang an einer gegebenen Position.

position() gibt die aktuelle Abspielposition in Millisekunden zurück.

rewind() spult die Datei an den Anfang zurück.

loop() veranlasst das Abspielen der Audiodatei in einer Schleife. Wenn kein Parameter übergeben wird, läuft diese Schleife endlos. Optional kann die Anzahl der Wiederholungen durch einen Wert vom Typ int angegeben werden.
```Java
    player.loop (); // Endlosschleife
    player.loop (7); // Spielt die Datei 7mal in Folge ab
```


### Aufgabe

Erstelle zwei Buttons zum Abspielen von zwei unterschiedlichen Sounddateien. Klickt man auf den ersten Button, soll der erste Soundeffekt abgespielt werden. Beim Klick auf den zweiten Button, soll der zweite Soundeffekt ertönen. Die Audiodateien sollen bei wiederholtem Klicken jeweils erneut abgespielt werden.

[Siehe MousePressed](https://processing.org/reference/mousePressed_.html)

[Siehe Conditions.](https://bitbucket.org/frescogamba/kickstartprog/src/3265989a4bfb80864af0d7003ae0648d43417635/004_conditions.md?at=master&fileviewer=file-view-default)

See [solution](solutions/sketch_047_minim.pde)

----
## Video library

Today we are writing a sketch that uses the Video Library. It is a basic Processing library which allows you to import movie files or capture images with a webcam in Processing.
So you will have to import the official Processing Video library and load it into your sketch.

### Capture Webcam

First import the library to be able to use the capture function:
```Java
import processing.video.*;

Capture video;
```
Capture is the class from the library, don't change this! The second part is the instance we create, you can name it whatever you like.

Now set up the sketch:
```Java
void setup(){
  size(640,400);
  video = new Capture(this, width, height);
  video.start();
  noStroke();
  smooth();
    }
```
The sketch size should be in the aspect ratio of your webcam, otherwise it will distort the image.
the Class Capture expects 3 parameters. First your device, which can be chosen by just using

```Java
this
```
The other two define the size of the webcam inside your sketch.
```Java
    video.start();
```
initiates the capturing.

Now, that our sketch gives us an image output we have to tell him to show us the picture, that the webcam is capturing.
This should happen in the draw function:

```Java
void draw(){
  if(video.available()){
    video.read();
    image(video, 0, 0, width, height);
  }
}
```
The if statement checks if the video source is available. The
```Java
video.read()
```
function reads the image.
And last but not least we need to declare where the image should be shown and what size it should have. For this we use
```Java
image();
```
which expects the source, position and size of the image.

The sketch should now look something like this and should be able to display the image of your webcam. Try it out.
```Java
import processing.video.*;

Capture video;

  void setup(){
    size(640,400);
    video = new Capture(this, width, height);
    video.start();
    noStroke();
    smooth();
}

void draw(){
  if(video.available()){
  video.read();
  image(video, 0, 0, width, height);
  }
}
```
Now try to write a sketch that multiplies the number of images when pressing a key. For example, you see just one image, if you press spacebar you see 4, then 16 and so on.  

Tip, for the multiplying webcams you can use for loops to create this pattern, like we did once before with ellipses (double for-Loop)

See [solution](solutions/sketch_048_video.pde)

----
## Fontastic

### Installation

Die Library [Fontastic](http://code.andreaskoller.com/libraries/fontastic/) kann im Contribution Mangager installiert werden.

Um mit Fontastic zu arbeiten muss die Library zu oberst im Sketch importiert werden:

``` java
import fontastic.*;
```

Danach kann man ein neues Objekt erstellen:

``` java
Fontastic f = new Fontastic(this, "ExampleFont");  // Create a new Fontastic object
```

### Glyph erstellen

Um Glyphen mit Fontastic anzuzeigen wird die Class [PVector](https://processing.org/reference/PVector.html) benötigt. Damit werden in uns unserem Fall die Punkte des Glyphen angegeben, welche dann verbunden werden und die Kontur ergeben. Der Funktion PVector() übergibt man zwei Parameter: die x- und y-Position des einzelnen Punktes:

```java
PVector[] points = new PVector[4];  // Define a PVector array containing the points of
points[0] = new PVector(0, 0);      // Start at bottom left
points[1] = new PVector(512, 0);    // The normal width is 512, the normal height 1024
points[2] = new PVector(220, 1024); // y coordinates are from bottom to top!
points[3] = new PVector(200, 1024);
```

Nun sind die 4 Punkte im Array _points_ gespeichert.
Jetzt muss diese "Kontur" noch einem Buchstaben zugewiesen werden:

``` java
f.addGlyph('A').addContour(points); // Assign contour to character A
```

Damit wird im Fontastic-Objekt f dem Buchstaben A die definierte Kontur zugewiesen.

Mit diesen zwei Schritten können dem ganzen Alphabet Konturen assoziiert werden.
Im Hintergrund wird für für jeden erstellen Glyph eine Datei im _data_-Folder erstellt.


### TrueType Font File erstellen

Um sein Meisterwerk zu vollenden, kann die erstellt Schrift als TrueType (.ttf) exportiert werden. Zusätzlich wird auch eine .woff-Datei erstellt. Das ist ein komprimierendes Containerformat für Webfonts.

```java
f.buildFont();                      // Build the font resulting in .ttf and .woff files
```

### Temporäre Dateien löschen

Zum Abschluss können die temporären Dateien im _data_-Folder gelöscht werden:

```java
f.cleanup();                  // Deletes all the files that doubletype created, except the .ttf and
							  // .woff files and the HTML template
```

### Auswahl an Font Eigenschaften

``` java
f.setAuthor("Andreas Koller");                  // Set author name - will be saved in TTF file too
f.setCopyrightYear(2017)  						// Sets the copyright year of the font.
f.setTypefaceLicense(String typefaceLicense) 	// Sets the license of the font
                         						// (default is "CC BY-SA 3.0 http://creativecommons.org/licenses/by-sa/3.0/")
f.setVersion(1.0)              					// Sets the version of the font (default is "0.1").
```
Alle weitere Eigenschaften kann in der [Dokumentation von Fontastic](http://code.andreaskoller.com/libraries/fontastic/#documentation) gefunden werden.


Versuche für jeden Buchstabe einen zufällige Form mit **PVector()** zu erstellen und diese dem Glpyh zuzuweisen. Die Form soll vier oder mehr zufällige Eckpunkte haben.
Das Ergebnis kann so aussehen:

![Zufälliger Font](https://bytebucket.org/frescogamba/kickstartprog/raw/e1ced186da7bbe40b11a68004ab39fa7193d01c6/misc/zufaelliger_font.png)

[Mögliche Lösung](solutions/sketch_049_fontastic.pde)

----

## Controlp5 - Create a „Paint Light”

### Introduction: Erklärung - Was tut ControlP5?

ControlP5 bietet verschiedene Controller-Optionen, um grafische User Interfaces zu kreieren. Du kannst zwischen Sliders, Buttons, Toggles, Knobs, Textfields, RadioButtons, Checkboxes und anderen auswählen und diese in deinen Processing Sketch integrieren. Für unser „Paint Light” Programm werden wir nur ein bis zwei Möglichkeiten der ControlP5 Library auswählen (siehe Definition der Tools).


### Download der Library

Bitte klicke auf den Link, um die Library herunterzuladen:

[controlP5.zip](https://github.com/sojamo/controlp5/releases/download/v2.2.5/controlP5-2.2.5.zip)

Verschiebe die library nun in den libraries folder:

documents > processing > libraries

Bevor wir mit dem Aufbau des „Paint Light” Programms beginnen, probiert einige der Beispiele aus. Diese findet ihr hier:

documents > processing > libraries > ControlP5 > examples > controllers


### Import der Library in den Sketch

Nun beginnen wir mit der Erstellung unseres kleinen paint-light-tools.

Importiert dazu die ControlP5 in ein neu erstelltes Processing-File. Dazu gehen wir über das Menu in Processing:
Sketch > Library importieren > ControlP5. In deinem Sketch sollte jetzt folgende Zeile stehen:

```java
import controlP5.*;
```



###Definition der Tools für unser „Paint Light”

Alles was das Paint Light Tool können muss, ist mit einem Pinsel malen. Die Farbe des Pinsels wird über ein Colorwheel dynamisch verändert und die Grösse und die Deckkraft der Pinselspitze über zwei Slider-Elemente festgelegt.


### Laden der Tools

Zunächst erstellen wir zwei Klassen für die zwei unterschiedlichen Elemente, die wir für unser kleines Programm benötigen (Colorwheel & Slider). Wir nennen sie cp5_colorwheel & cp5_slider.

```java
ControlP5 cp5_brush;
ControlP5 cp5_slider;
```


### Erstellen der Tools

Nun müssen wir Instanzen der zuvor erstellten Klassen erzeugen.
Erinnere dich, wir benötigen:
Colorwheel (für die Farbe des Pinsels)
Slider (für die Opacity des Pinsels)
Slider (für die Grösse des Pinsels)

Im Zusammenspiel mit der ControlP5 library stellen wird dies wie folgt an:

```java
void setup() {
  size(1000, 1000);

  //Color Wheel
  cp5_colorwheel = new ControlP5(this);
  cp5_colorwheel.addColorWheel("colorwheel", 250, 10, 200 ).setRGB(color(255, 0, 0));

  //Slider
  cp5_slider = new ControlP5(this);
  cp5_slider.begin(100, 100);
  cp5_slider.addSlider("size", 10, 200).linebreak();
  cp5_slider.addSlider("opacity", 0, 255);

  smooth();
  noStroke();
  background(0);
}

int colorwheel = color(0);
float size = 20, opacity = 255;
```


Starte nun den Sketch und sieh dir an wie dein Produkt bis jetzt aussieht. Die Kontrollelemente sollten alle vorhanden sein, nun muss noch die Funktionalität gewährleistet werden.

### Verbinden der Tools „Slider” und „ColorWheel” mit unserem Pinsel

Nun müssen wir noch einbinden, dass wir mit der Maus zeichnen können. Wie könnte man das anstellen? Versuche selber auf die Lösung zu kommen.
Tipp: die Variablen in die wir die Farb, Deckkraft und Grössen -Werte abspeichern heissen wie folgt:
Farbe:
```java
colorwheel
```

Deckkraft:
```java
opacity
```

Grösse:
```java
size
```

Die Lösung für die Faulen: [Lösung](solutions/sketch_049_paintlightp5.pde)
