
import ddf.minim.*;

Minim minim;
AudioPlayer fart;
AudioPlayer meow;


void setup()
{
  size(300, 150, P3D);
  minim = new Minim(this);
  fart = minim.loadFile("Fart-sound-effect.mp3");
  meow = minim.loadFile("cat.mp3");
}

void draw()
{
  background(0);
  fill(255,0,0);
  ellipse(100,75,50,50);
  fill(0,0,255);
  ellipse(200,75,50,50);

}

void mousePressed(){
  if(mouseX > 75 && mouseX < 125 && mouseY > 50 && mouseY < 100){
  fart.rewind();
  fart.play();
  }
  if(mouseX > 175 && mouseX < 225 && mouseY > 50 && mouseY < 100){
  meow.rewind();
  meow.play();
  }
}
