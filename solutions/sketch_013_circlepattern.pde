void setup() {
  size(500, 500);
  background(255);
  pixelDensity(2);
  noStroke();
}
void draw() {
  int steps = 50;
  int yOffset = 50;
  fill (220);
  for (int x = 50; x < width; x += steps) {
    for (int y = yOffset; y < height; y += steps) {
      ellipse(x, y, 30, 30);
    }
  }
}
