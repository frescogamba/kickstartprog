void setup() {
  size(640,480);
}

void draw() {
  // center
  point(320,240);
  // top left
  point(0,0);
  // top right
  point(640,0);
  // bottom left
  point(0,480);
  // bottom right
  point(640,480);
}
