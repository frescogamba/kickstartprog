void setup() {
  // for retina displays
  pixelDensity(2);
  size(640,480);
}

void draw() {
  fill(0);
  // Angles must be specified in radians (values from 0 to TWO_PI)
  rotate(0.4);
  rect(140,200,40,40);
}
