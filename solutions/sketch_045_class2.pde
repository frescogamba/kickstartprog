Ball b;
Ball b1;

void setup() {
  size(400, 400);
  b = new Ball(10, 10, 30);
  b1 = new Ball(100, 100, 50);
}

void draw() {
  background(255);
  b.move();
  b.render();
}

class Ball {
  float x;
  float y;
  float size;

  Ball(float _x, float _y, float _size) {
    // now we need to assign these new variables to the ones we created in the first place
    x = _x;
    y = _y;
    size = _size;
  }

  void move() {
    x = x + 5;
  }

  void render() {
    ellipse(x, y, size, size);
  }
}
