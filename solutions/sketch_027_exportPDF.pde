import processing.pdf.*;

void setup() {
  size (500, 800, PDF, "poster.pdf");
  background(0);
  noLoop();
  noStroke();
}

void draw() {
  int anzahlkreise = 20;
  int kreisgroesse = 50;

  for (int i=0; i<anzahlkreise; i++) {
    fill(random(255), random(255), random(255));

    // kreise ueberall
    float x = random(width);
    float y = random(height);

    // kreise zentriert
    //float x = random(width * 0.3, width * 0.7);
    //float y = random(height * 0.3, height * 0.7);

    println(x, y);
    ellipse(x, y, kreisgroesse, kreisgroesse);
  }

  // auge
  fill(255);
  ellipse(width/2, height/2, width/3, height/10);
  fill(139, 69, 19);
  ellipse(width/2, height/2, width/8, width/8);
  exit();
}
