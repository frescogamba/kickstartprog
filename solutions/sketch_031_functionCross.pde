void setup() {
  size(1000, 600);
}

void draw() {
  kreuz(); // here we make the call to the function
}

void kreuz() {
  line(mouseX, mouseY, mouseX+100, mouseY+100);
  line(mouseX, mouseY+100, mouseX+100, mouseY);
  /* to draw the cross at the center
  line(mouseX-50, mouseY-50, mouseX+50, mouseY+50);
  line(mouseX-50, mouseY+50, mouseX+50, mouseY-50);
  */
}
