import controlP5.*;

ControlP5 cp5_colorwheel;
ControlP5 cp5_slider;

void setup() {
  size(1000, 1000);
  
  //Color Wheel
  cp5_colorwheel = new ControlP5(this);
  cp5_colorwheel.addColorWheel("colorwheel", 250, 10, 200 ).setRGB(color(255, 0, 0));

  //Slider
  cp5_slider = new ControlP5(this);
  cp5_slider.begin(100, 100);
  cp5_slider.addSlider("size", 10, 200).linebreak();
  cp5_slider.addSlider("opacity", 0, 255);

  smooth();
  noStroke();
  background(0);
}

int colorwheel = color(0);
float size = 20, opacity = 255;

void draw() {
  if (mousePressed) {
    fill(colorwheel, opacity);
    ellipse(mouseX, mouseY, size, size);
  }
}