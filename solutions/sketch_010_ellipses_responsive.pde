void setup() {
  // for retina displays
  pixelDensity(2);
  size(800,500);
  background(255);
  noStroke();
}

void draw() {
  int y = height/2;
  int d = height/4;
  int x = width/6;
  int distance = width/6;

  fill(128);
  ellipse(x, y, d, d); // Links
  ellipse(x+distance, y, d, d); // Center
  ellipse(x+distance * 2, y, d, d); // Rechts
}
