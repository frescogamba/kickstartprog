void setup() {
  size(500, 500);
  background(255);
  pixelDensity(2);
  noStroke();
}
void draw() {
  int numberofRows = 5;
  int steps = width/ numberofRows;
  int yOffset = steps;
  fill (220);
  for (int x = steps; x < width; x += steps) {
    for (int y = yOffset; y < height; y += steps) {
      ellipse(x, y, steps /2 , steps/2);
    }
  }
}
