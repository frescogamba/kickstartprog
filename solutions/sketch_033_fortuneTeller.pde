void setup() {
  size(1000, 600);
  background(255);
  textSize(30);
  fill(0);
}

void draw() {
  // detect mouse click
   if (mousePressed) {
     // reset background
     background(255);
     // call function fortune with number 6
     fortune(6);
   }
}

void fortune(int amount) {
  int randomNumber = int(random(amount)); // create a random number
  if (randomNumber == 1) {
    text("Today it's up to you to create the peacefulness you long for.", 0,100);
  }
  else if (randomNumber == 2) {
    text("A smile is your passport into the hearts of others.", 0,100);
  }
  else if (randomNumber == 3) {
    text("Change can hurt, but it leads a path to something better.", 0,100);
  }
  else if (randomNumber == 4) {
    text("You learn from your mistakes... You will learn a lot today.", 0,100);
  }
  else if (randomNumber == 5) {
    text("The greatest risk is not taking one.", 0,100);
  }
  else {
    text("Sorry, no fortune for you...", 0,100);
  }
}
