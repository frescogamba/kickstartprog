import geomerative.*;
RFont font;

void setup() {
  size(500, 500);
  background(255);
  pixelDensity(2);

  RG.init(this); //Initialise the library.

  // we are loading the FreeSans.ttf that's inside the data folder, 100 determines the font size
  font = new RFont("FreeSans.ttf", 100, LEFT);

  RCommand.setSegmentLength(10); //Assign a value of 10, so every 10 pixels.
  RCommand.setSegmentator(RCommand.UNIFORMLENGTH);
}

void draw() {
  background(255);
  //Group together font with text.
  RGroup myGroup = font.toGroup("hello world");
  myGroup = myGroup.toPolygonGroup();

  RPoint[] myPoints = myGroup.getPoints();

  translate(10, height/2);
  for (int i=0; i<myPoints.length; i++) {
    ellipse(myPoints[i].x, myPoints[i].y, 5, 5);
  }
}
