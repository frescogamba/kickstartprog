void setup() {
  size(500, 500);
  background(255);
  pixelDensity(2);
  noStroke();
}
void draw() {
  float numberofRows = 15;
  float steps = width/ numberofRows;
  fill (50);
  for (float x = steps / 2; x < width; x += steps) {
    for (float y = steps /2; y < height; y += steps) {
      ellipse(x, y, steps, steps);
    }
  }
}
