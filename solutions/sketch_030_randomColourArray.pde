// define an array with integer values
//int[] colors = { 0, 128, 200, 255, 100};
color[] colors = { #FF0000, #FFC000, #E0FF00, #7EFF00, #21FF00 };
void setup() {
  size(1000, 1000);
  pixelDensity(2);
}

void draw() {
  background(255);
  translate(width/2, height/2);
  float randomIndex = random(5);
  fill(colors[int(randomIndex)]);
  ellipse(0, 0, 100, 100);
  delay(500);
}
