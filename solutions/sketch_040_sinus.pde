void setup() {
  size(800, 800);
  pixelDensity(2);
}
float angle = 0;
float speed = 1;

void draw() {
  rectMode(CENTER);
  translate(width/2, height/2);
  angle += speed;
  float y = 100 * sin(radians(angle));
  float x = 100 * cos(radians(angle));
  translate(x,y);
  rotate(radians(angle));
  rect(0,0,40,20);
}
