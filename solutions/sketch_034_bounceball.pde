float xpos, ypos;
float xspeed=3;
//float yspeed=4;

void setup() {
  size(400, 400);
  frameRate(60);
}
void draw() {
  background(0);
  rect(xpos, ypos, 10, 10);
  xpos+=xspeed;
  if (xpos>=width-10 || xpos<=0) {
    xspeed*=-1;
  }
}
