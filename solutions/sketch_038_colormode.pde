void setup() {
  size(360, 100);
}
void draw() {
  noStroke();
  colorMode(HSB, 360, 100, 100);
  for (int i = 0; i < 360; i++) {
    for (int j = 0; j < 100; j++) {
      stroke(i, j, 100);
      point(i, j);
    }
  }
}
