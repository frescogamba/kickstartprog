void setup() {
  size(500, 500);
  background(255);
  pixelDensity(2);
  textSize(40);
  fill(0);
}

float angle = 0;
void draw() {
  angle += 1;
  println(radians(angle));
  translate(width/2, height/2);
  rotate(radians(angle));
  text("uncertainty", 0, 0);
  if (angle > 360) {
    angle = 0;
    noLoop();
  }
}
