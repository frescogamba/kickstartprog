void setup() {
  size(1000, 1000);
  background(255);
  pixelDensity(2);
  textSize(20);
  fill(0);
}

float angle = 0;
void draw() {
  angle += 1;
  translate(width/2,height/2);
  rotate(radians(angle));
  translate(angle/2,angle/2);
  String myText = "uncertainty";
  text("uncertainty", 0, 0);
  if (angle > 360) {
    angle = 0;
    noLoop();
  }
}
