void setup() {
  size(1000, 600);
}

void draw() {
  background(255);
  kreuz(50); // here we make the call to the function
  kreuz(10);
  kreuz(100);
}

void kreuz(int size) {
  line(mouseX, mouseY, mouseX+size, mouseY+size);
  line(mouseX, mouseY+size, mouseX+size, mouseY );
}
