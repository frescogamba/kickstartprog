void setup() {
  size(400, 400);
  frameRate(60);
}
float tnoise = 0;
float x;

void draw() {
  x += 1;
  tnoise += 0.01;
  line(x, height/2, x, height/2 - random(height / 2));
  translate(0,height/2);
  line(x, height/2, x, height/2 - height/2 * noise(tnoise));
}
