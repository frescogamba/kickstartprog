Ball b;

void setup() {
  size(400, 400);
  b = new Ball();
}

void draw() {
  background(255);
  println(b.x);
  ellipse(b.x, b.y, b.size, b.size);
}

class Ball {
  float x;
  float y;
  float size;

  Ball() {
    x = 10;
    y = 10;
    size = 30;
  }
}
