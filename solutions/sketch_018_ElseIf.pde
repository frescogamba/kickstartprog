
void setup() {
  size(500, 500);
  background(128);
  stroke(0);
}

void draw() {
  background(128);
  if (mouseX < width/2 && mouseY < height /2 ) {
    background(0, 255, 0); // wenn maus position < 100
    println("oben links");
  } else if (mouseX > width /2 && mouseY < height /2 ) {
    background(0, 0, 255); // wenn maus position < 100
    println("oben rechts");
  } else if (mouseX < width /2 && mouseY > height/2) {
    background(255,0,0);
    println("unten links");
  }
  else {
    background(255);
    println("unten rechts");
  }
  if (mousePressed) {
    background(0);
  }
  line(0, height/2, width, height/2);
  line(width/2, 0, width/2, height);
}