import geomerative.*;
RFont font;

void setup() {
  size(500,500);
  background(255);
  pixelDensity(2);
  RG.init(this); //Initialise the library.

  // we are loading the FreeSans.ttf that's inside the data folder, 100 determines the font size
  font = new RFont("FreeSans.ttf", 100, LEFT);
}

void draw() {
  background(255);
  translate(10, height/2);
  font.draw("hello world");
}
