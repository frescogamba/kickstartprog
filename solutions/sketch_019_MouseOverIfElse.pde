float x, y, w, h;

void setup() {
  size(500, 500);
  x = width/2;
  y = height/2;
  w = 100;
  h = 40;
  background(255);
}
void draw() {
  if ((mouseX > x) && (mouseX < x+w) &&
    (mouseY > y) && (mouseY < y+h)) {
    fill(0);
  } else {
    fill(255);
  }
  rect(x, y, 100, 40);
}
