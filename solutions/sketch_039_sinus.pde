void setup() {
  size(800, 800);
  pixelDensity(2);
}
float angle = 0;
float speed = 1;

void draw() {
  translate(width/2, height/2);
  angle += speed;
  float y = 100 * sin(radians(angle));
  float x = 100 * cos(radians(angle));
  point(x, y);
  println(x, y);
}
