void setup() {
  // for retina displays
  pixelDensity(2);
  size(640,480);
}

void draw() {
  // from top left to bottom right
  line(0,0,640,480);
  // from top right to bottom left
  line(640,0,0,480);
}
