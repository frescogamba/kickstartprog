void setup() {
  // for retina displays
  pixelDensity(2);
  size(600,200);
  background(255);
  noStroke();
}

void draw() {
  int y = 100;
  int d = 60;
  int x = 100;
  fill(128);
  ellipse(x, y, d, d); // Links
  ellipse(x+100, y, d, d); // Center
  ellipse(x+200, y, d, d); // Rechts
}
