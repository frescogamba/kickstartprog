import processing.video.*;

Capture cam;
int anzahl = 1;

void setup() {
  size(640, 400);

 cam = new Capture(this, width, height);
  cam.start();
}

void draw() {
 if (cam.available() == true) {
    cam.read();
  }

 //image(cam, 0, 0);

 for(int i = 0; i <= anzahl; i++){
    for(int j = 0; j <= anzahl; j++){
      int camwidth = width/anzahl;
      int camheight = height/anzahl;
      image(cam, i * camwidth, j*camheight, camwidth, camheight);
    }
  }
}

void keyPressed() {
  if (key == ' ') {
    anzahl = anzahl * 2;
    if (anzahl==64){
      anzahl=1;
    }
  }
}
