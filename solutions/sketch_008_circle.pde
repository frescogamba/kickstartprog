void setup() {
  size(640,480);
  noStroke();
  background(0);
  pixelDensity(2);
}

void draw() {
  ellipse(width/2, height/2, width/3, width/3);
}
