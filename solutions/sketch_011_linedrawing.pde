void setup() {
  size(500, 500);
  background(255);
  pixelDensity(2);
}
void draw() {
  int steps = 5;
  for (int i = 0; i <= 500; i = i + steps) {
    line(0, i, width, height);
  }
}
