import fontastic.*;

Fontastic f;

void setup() {
  size(600, 400);
  background(255);
  fill(0);
  noLoop();
  createFont(); // create the font
}


void draw() {

  PFont myFont = createFont(f.getTTFfilename(), 64); // reading the font that has just been created

  textFont(myFont);
  textAlign(CENTER, CENTER);
  text(Fontastic.alphabet, 0, Fontastic.alphabet.length/2, width/2, height/5);
  text(Fontastic.alphabet, Fontastic.alphabet.length/2, Fontastic.alphabet.length, width/2, height/5*2);

  text(Fontastic.alphabetLc, 0, Fontastic.alphabet.length/2, width/2, height/5*3);
  text(Fontastic.alphabetLc, Fontastic.alphabet.length/2, Fontastic.alphabet.length, width/2, height/5*4);
}

void createFont() {

  if (f != null) { 
    f.cleanup();
  }

  f = new Fontastic(this, "RandomFont");

  f.setVersion("1.0");
  f.setAdvanceWidth(600);

  int numberOfPoints = 4;

  // go through alphabet and create contours for each glyph
  for (char c : Fontastic.alphabet) {

    // create a PVector array with 4 random coordinates
    PVector[] points = new PVector[numberOfPoints];

    for (int i=0; i<numberOfPoints; i++) {
      points[i] = new PVector(random(512), random(1024));
    }

    f.addGlyph(c).addContour(points);

    PVector[] pointsLc = new PVector[points.length];

    // and to the same for lowercase characters. The contour gets scaled down by 50% in y.
    for (int i=0; i<pointsLc.length; i++) {
      pointsLc[i] = new PVector();
      pointsLc[i].x = points[i].x;
      pointsLc[i].y = points[i].y * 0.5;
    }

    f.addGlyph(Character.toLowerCase(c)).addContour(pointsLc);
  }

  f.buildFont();
  f.cleanup();
}