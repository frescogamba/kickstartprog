Ball b;
Ball b1;

void setup() {
  size(400, 400);
  b = new Ball(10, 10, 30, 1);
  b1 = new Ball(100, 100, 50, 5);
}

void draw() {
  background(255);
  b.move();
  b.render();
  b.checkBorders();
  b1.move();
  b1.render();
  b1.checkBorders();
}

class Ball {
  float x;
  float y;
  float size;
  float speed;

  Ball(float _x, float _y, float _size, float _speed) {
    // now we need to assign these new variables to the ones we created in the first place
    x = _x;
    y = _y;
    size = _size;
    speed = _speed;
  }

  void move() {
    x = x + speed;
  }

  void render() {
    ellipse(x, y, size, size);
  }

  // create new function that detects if the circle left the window width
  void checkBorders() {
    if (x > width || x < 0) {
      // we invert the speed. it becomes a negative value and the ball will bounce off the wall
      speed = speed * -1;
    }
  }
}
