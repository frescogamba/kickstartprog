void setup() {
  size(1000, 1000);
  background(255);
  pixelDensity(2);
  fill(0);
}

void draw() {
  rect(mouseX, mouseY, 100, 100);
  if (mousePressed) {
    saveFrame("test-#####.jpg");
  }
}
