### Functions

Functions are essential building blocks and we've used quite a couple of them already (setup() and draw()). Think of the line() function that draws a line onto the screen. Every time we were calling line(), we were calling this function together with four parameters inside of the parenthesis. This function is built-in in Processing. The nice thing is that we can define our own functions; let's say for example we want to draw more complex shapes and forms. The concept of functions is a powerful one, because it allows you to write modular code. They also allow you to think in abstractions, i. e. you define for example a star function that draws a star to the screen. You don't need to know the exact details how to design a star; calling the function with a parameter will do the trick.

You can define a new function with the **void()** syntax outside of the setup() and draw() functions.

Let's build a function that draws a cross onto the screen:

```java
void kreuz() {
  line(0, 0, 100, 100);
  line(0, 100, 100, 0);
}
```

Everything inside the {} curly braces is belonging to this function. You can define the function on the top or the bottom of your sketch. It doesn't matter where you put it exactly as long as it is outside of setup() and draw(). Usually, we put it after the draw() function.

Looking onto our fortune teller example we could define a new function **fortune()** with the void statement:

```java
void setup() {
  size(1000, 600);
}

void draw() {
  kreuz(); // here we make the call to the function
}

void kreuz() {
  line(0, 0, 100, 100);
  line(0, 100, 100, 0);
}
```

Can you adapt the code so the cross is drawn exactly at the current mouseposition?

See [solution](solutions/sketch_031_functionCross.pde)

We can add more modularity to our function if we pass arguments to it. For example, we would like to control the size of our cross by passing an additional parameter inside of the parenthesis while calling the function:

```java
void draw() {
  kreuz(50); // calling the function with one parameter
}
```

We have to reflect this additional argument in our function definition:

```java
void kreuz(int size) {
  line(0, 0, size, size);
  line(0, size, size, 0);
}
```

Try this and try calling the kreuz() function multiple times with different parameters. We should add the mouseX and mouseY variables to make the cross appear at the mouse position first.

See [solution](solutions/sketch_032_functionCrossParameter.pde)

Imagine we would like to pass also an argument that defines the colour while calling the function. We can separate multiple arguments with a comma:

```java
void draw() {
  kreuz(50,128,30,40); // calling the function with multiple parameters
}
```

Again, we need to adapt our function definition for the three additional parameters:

```java
void kreuz(int size, int r, int g, int b) {
  stroke(r,g,b);
  line(0, 0, size, size);
  line(0, size, size, 0);
}
```

#### Fortune teller

We can use functions to clean up our code inside the draw function. Let's take a look at the fortune teller example. Can you understand this code? Can you add more fortune sentences?

```java
void setup() {
  size(1000, 600);
  background(255);
  textSize(30);
  fill(0);
}

void draw() {
  // detect mouse click
   if (mousePressed) {
     // reset background
     background(255);
     // call function fortune with number 6
     fortune(6);
   }
}

void fortune(int amount) {
  int randomNumber = int(random(amount)); // create a random number
  if (randomNumber == 1) {
    text("Today it's up to you to create the peacefulness you long for.", 0,100);
  }
  else {
    text("Sorry, no fortune for you...", 0,100);
  }
}
```

See [solution](solutions/sketch_033_fortuneTeller.pde)

#### Star function

```java
void setup() {
  size(600, 600);
  background(255);
}

void draw() {
   star();
}

void star() {
  translate(mouseX,mouseY);
  beginShape();
  vertex(0, -50);
  vertex(14, -20);
  vertex(47, -15);
  endShape(CLOSE);
}
```

With **beginShape()** we are opening up a shape which is followed by individual points called **vertex(x,y)** Can you complete the shape, so it looks like a star?

See [solution](solutions/sketch_020_rotate.pde).

**translate()** is another new function which we will discover in the next section.

We can also pass additional parameters inside the parenthesis. The number of parameters you are putting inside the parenthesis must match the number of parameters you initially define with **void()**.

```java
void draw() {
   star(128);
}

void star(int colour) {
  translate(mouseX,mouseY);
  fill(colour);
  beginShape();
  vertex(0, -50);
  vertex(14, -20);
  vertex(47, -15);
  endShape(CLOSE);
}
```

## Translate, scale, rotate via push and pop

### Translate

Up till now, we have always assumed that the coordinate system is starting from top left with the coordinates 0,0. Another technique for positioning and moving things is to change the coordinate system. We can move the default coordinate system with the function **translate()** which takes two parameters (the x and y value). It's like an offset. First, try this:

```java
void setup() {
  size(500, 500);
  background(255);
  pixelDensity(2);
  textSize(40);
  fill(0);
}

void draw() {
  background(255);
  translate(mouseX, mouseY);
  text("uncertainty", 0, 0);
}
```

The translate() function sets the coordinate system of the screen to the mouse location (mouseX and mouseY).

### Rotate

The **rotate()** function rotates the coordinate system. It takes exactly one parameter (the angle in radians). If you want to work with numeric angle parameters (from 0 to 360), you'll to transform the angle from degrees to radians. Read more about conversion [here](https://processing.org/reference/radians_.html). A more profound explanation of the unit radian can be found [here](https://en.wikipedia.org/wiki/Radian)

Or try it yourself to see what is the radians value of 90 degrees:

```java
void draw() {
  println(radians(90));
}
```

Now, we will try to save the variable __angle__ and increment it each time the draw() function is being called.

```java
float angle = 0;
void draw() {
  angle += 1;
  println(radians(angle));
}
```

Can we tell the program that it should __reset__ the angle variable once it's bigger than 360?

See [solution](solutions/sketch_020_rotate.pde)

Next challenge: Can you make rotate the text we've used before? Try to experiment with both, the translate and the rotate function.

See [solution](solutions/sketch_021_translate_rotate_text.pde)

You'll quickly notice that the centre of rotation is the top left corner. What if we want to rotate around the centre of the object? We will have to offset the the coordinates of the text function in this line:

```java
text("uncertainty", 0, 0);
```

The function [textWidth](https://processing.org/reference/textWidth_.html) helps us to find out the width of a string. We have to store the text string in a variable in order to get its width:

```java
String myText = "uncertainty";
float w = textWidth(myText);
println(w);
```

We know the width of our text in pixels. Now we can offset it by exactly half of that value.

```java
String myText = "uncertainty";
float w = textWidth(myText);
text("uncertainty", -w/2, 0);
```

The y value should also be offset by the height of the font divided by two. We initially took 40 within the textSize function. Hence, we can write **-20** to offset the appearance of the text.

```java
String myText = "uncertainty";
float w = textWidth(myText);
text("uncertainty", -w/2, -20);
```

See [solution](solutions/sketch_022_translate_rotate_center_text.pde)

So far, we used the translation function before calling rotate. What happens when you change the order? Try rotating first and then translate. What happens if you for example use a variable that grows over time inside the translate function?

See [solution](solutions/sketch_023_rotate_translate_text.pde)

### Scale

As you might guess, the scale function, scales and stretches the coordinate system. The value given is in floating percentages, which means if you pass 1.2 it stretches the system by 120%, passing 2 is equal to 200% (double size), etc.

Try to take the example with the centred text and make it grow. It is growing very quickly. What parameter do we need to change it make it grow slower?

See [solution](solutions/sketch_024_scale.pde)

Can you make the text fade in? Use the fill function it make it dependent on the variable that is growing together with the scale function:

See [solution](solutions/sketch_025_scale_fade.pde)

### PushMatrix and popMatrix

As we've seen, the translate, rotate and scale function affect all other later drawing functions in our sketch. We can isolate the all these transformations, so they **don't** affect our coordinate system. With **pushMatrix()** you can __save__ the current coordinate system which can be later restored with **popMatrix()**. This is handy if you want to transform some operations onto one object but not on other ones inside your sketch.

Let's say, I want to make the rotating text example and draw a square in the centre of the sketch (that doesn't rotate).

```java
void setup() {
  size(1000, 1000);
  background(255);
  pixelDensity(2);
  textSize(20);
  fill(0);
}

float speed = 0;
void draw() {
  speed += 0.1;
  pushMatrix();
  translate(0,height/2);
  scale(speed);
  fill(0,0,0,(speed));
  String myText = "uncertainty";
  text("uncertainty", 0, 0);
  popMatrix();
  rect(0,0,100,100);
}
```
**pushMatrix()** and **popMatrix** are always used in pairs. For each **pushMatrix()** you'll have to call a matching **popMatrix()**.

Challenge: Try to comment out the pushMatrix and the popMatrix() functions. What happens? Try to perform another transformation by using pushMatrix and popMatrix around the square.

What you might have noticed is that the fill function affects both objects. pushMatrix and popMatrix don't affect the styling of objects. You can use **pushStyle()** and **popStyle()** to handle this.

```java
void draw() {
  speed += 0.1;
  pushMatrix();
  translate(0,height/2);
  scale(speed);
  pushStyle();
  fill(0,0,0,speed);
  String myText = "uncertainty";
  text("uncertainty", 0, 0);
  popStyle();
  popMatrix();
  rect(0,0,100,100);
}
```

The fill() of the rectangle isn't affected anymore.

## Arrays

Arrays are a list of variables. You can store multiple values inside of an array. Each item stored in an array is typically called element and has an index that defines its position. The first element inside of an array has the index 0, the second one has the index 1 and so on; the last element has the index equal to the number of all items minus 1. We can for example store a colour palette inside of an array:

```java
int[] colors = { 0,128,200,255,100 };
```

Arrays are defined by using first the datatype (in this case it's an array of int elements) and then the [] brackets. The name of the array is __colors__ and it contains five elements. Now, if we want to access the first element for example, we could simply write:

```java
void setup() {
  size(1000, 1000);
  background(colors[0]);
  pixelDensity(2);
}
```

With __colors[0]__ we are accessing the first element of the array which holds the value 0 (i. e. black). What index number do we need to access the white number?

If we want to randomly access one of the array elements, we can again make use the random() function:

```java
void draw() {
  background(255);
  float randomIndex = random(5);
  println(randomIndex);
}
```

Let's look at the output of the console. We can observe that the numbers are unfortunately floating numbers, but the index number of the array needs to be an integer. We can make use of another function that automatically transforms floating numbers into integer. It's called **int()** and takes one argument (i. e. the number that it should convert and it returns the same number without a decimal point)

```java
void draw() {
  background(255);
  float randomIndex = random(5);
  println(int(randomIndex));
}
```

Now we can try to see if we can get the colour values:
```java
void draw() {
  background(255);
  float randomIndex = random(5);
  println(colors[int(randomIndex)]);
}
```

You should see the colour values popping up in the console. Can you adapt the code, so an ellipse is getting a random fill from the colors array?

See [solution](solutions/sketch_029_randomColourArray.pde)

The colours in the array are only grayscale values. But, what if we want colours?

We can actually use a hexadecimal representation of colour values, very much like in web development. Read here for more information. An array with hexadecimal values would look like this:

```java
color[] colors = { #FF0000, #FFC000, #E0FF00, #7EFF00, #21FF00 };
```

To access the first element (#FF0000 = red)  we could write:
```java
fill(color[0]);
```

Can you adapt the previous example? Can you create your own colour palette?

See [solution](solutions/sketch_030_randomColourArray.pde)

## Saving your artwork

We can use different approaches in order to export your sketches into different formats. So far, we've only created screenshots which has its limits. The function **saveFrame()** saves one image each time the function is run. By adding an extension, you can determine the filetype. The parameter inside the parenthesis can be any sequence of letters or numbers that ends with either ".tif", ".tga", ".jpg", or ".png". Let's put this into or draw function:

```java
void setup() {
  size(1000, 1000);
  background(255);
  pixelDensity(2);
  fill(0);
}

void draw() {
  rect(mouseX,mouseY,100,100);
  saveFrame("test.jpg");
}
```

You'll have to save the sketch first. Now, you'll find a __test.jpg__ inside your sketch folder. By adding the # hashtag symbol to the filename to get a numbered sequence of your images. Try following:

```java
void draw() {
  rect(mouseX,mouseY,100,100);
  saveFrame("test-#####.jpg");
}
```

The sketch appears much slower since the computer needs some extra computation time to write to your hard-disk. Try to bind the saveFrame to an event, such as a **mousePressed** or **keyPressed** condition.

See [solution](solutions/sketch_026_saveFrameMouse.pde)

### Save directly in a PDF

Let's look onto our [sketch with the poster design](solutions/sketch_016_paulrand.pde) of Paul Rand's circles. Then, we'll look at some online information concerning the [PDF Export](https://processing.org/reference/libraries/pdf/index.html). We'll try the single frame export and modify our sketch accordingly.

First, we need to include an external PDF export library. In the very first line of our sketch we write:

```java
import processing.pdf.*;
```

Secondly, we need to adapt the size function inside **setup()**. We will add two more parameters, namely the filetype and filename:

```java
  size (500, 800, PDF, "poster.pdf");
```

This calls the size function with, opens a PDF called poster.pdf. (Note that no display window will open; this helps when you're trying to create massive PDF images that are far larger than the screen size).

Inside the draw function we need to tell the sketch that it should immediately exit once it has done the drawing. Hence, we call right before the closing curly brace } of the draw function, the **exit()**.

```java
  exit();
```

See [solution](solutions/sketch_027_exportPDF.pde)

Once, you saved the sketch and execute it, you'll find a PDF inside of your sketch folder. Try to open the file with Illustrator to see if the objects appear as vector shapes.
