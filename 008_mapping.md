# Math

## Mapping values

Often we have to work with value ranges that need to be scaled up or down. If we want to map the mouseX function to a colour value, we need to translate the range from possible mouseX values (0 - max. screen width, for example 1280px) to a range from 0 to 255. Therefore, processing offers us a function called **map()**.

The syntax looks like this:

```java
map(value, start1, stop1, start2, stop2)
```

The first parameter is the variable to be converted, the second and third parameters are the low and high values of that variable, and the fourth and fifth parameters are the desired low and high values. The map() function hides the math behind the conversion.

Let's see an example first without map:

```java
void setup() {
  size(1000, 400);
  noStroke();
}
void draw() {
  background(mouseX);
  println(mouseX);
}
```

Ideally, we would like to fade the background from white to black along the entire horizontal axis.

```java
void draw() {
  float c = map(mouseX, 0, width, 0, 255);
  println(c);
  background(c);
}
```

**mouseX** is taking as input value the range from 0 to sketch width (1000px) and recalculationg it proportionally to a new value range from 0 to 255.

Challenge: Try to apply the same for mouseY and make a appear a form that changes its appearance based on the mouse position. Try to use at least one or two map functions.

# Math (a little bit)

In our previous examples we used very often linear movement by incrementing a variable in order to perform a colour fade or a movement of an object. Linear movements often look a bit non-fluid and non-organic. Trigonometry gives us a lot of tools that can make nice ease-in and ease-out movements. We need to dig a little bit into basic geometry by looking closer to the **sin()** and **cos()** functions which are available in Processing. Just as a quick refresh exercise, what was the sine and cosine in math?

![Sinus](https://bytebucket.org/frescogamba/kickstartprog/raw/fb66850636c1257470fb5de91aa0d45f601f3134/misc/sin_cos.jpeg)

This [animation](https://en.wikipedia.org/wiki/Sine#/media/File:Circle_cos_sin.gif) shows the relationship between the sine and cosine. Let's assume we have a circle and we want to know the position of a point along the circle outline. The cosine function will return us the x (= horizontal) position of any given point. For example, at 0 degrees, the cosine function will return 1. If we want to move the point up while resting alongside the circle outline, its position is defined by the sine and cosine function whereas the sine function returns us the vertical distance to the center point of the circle and the cosine function returns us the horizontal distance. A much more detailled explanation of the relationship between those functions can be obtained through the Wikipedia entry of [Trigonometric_functions](https://en.wikipedia.org/wiki/Trigonometric_functions).

Let's start with an example:

```java
void draw() {
  float y = sin(90);
  println(y);
}
```

If we look into the documentation of the **sin()** function, we'll notice that it takes the angle as radians. Hence, we first need to convert our angle by using the radians function.

```java
  float y = sin(radians(90));
  println(y);
```

The value being shown in the console is 1. Try another value for the angle, for example 0. We also want to look at the cosine function:

```java
  float y = sin(radians(0));
  float x = cos(radians(0));
  println(x); // cos for 0 is 1
  println(y); // sin for 0 is 0
```

Imagine we want to draw a point at this position using the point() function. We'll use translate to set the center of our coordinate system to the center of the sketch.

```java
  translate(width/2,height/2);
  float y = sin(radians(0));
  float x = cos(radians(0));
  point(x,y);
  println(x,y);
```

Can you change the angle? Where does the point appear? Look at the console to see the values for x and y. What can you observe? Do you need to scale up or down those values? Try multiplying x and y with another value.

See [solution](solutions/sketch_039_sinus.pde)

Let's replace the point function with a rect function.

```java
//point(x,y);
//println(x,y);
rect(x,y,40,20);
```

We should see something like this:

![Sinus](https://bytebucket.org/frescogamba/kickstartprog/raw/fb66850636c1257470fb5de91aa0d45f601f3134/misc/sinus_rect.jpg)

Now, let's try to rotate the rectangles by adding a rotate() function, so that they are aligned towards the center of the circle.

```java
void draw() {
  translate(width/2, height/2);
  angle += speed;
  float y = 100 * sin(radians(angle));
  float x = 100 * cos(radians(angle));
  rotate(radians(45));
  rect(x,y,40,20);
}
```

The result looks good but isn't what we want to achieve. Try to place the angle variable inside the rotate function. What can you observe? Where's the center of the rotation?

If we want to rotate the rectangular right around its own point of orientation we have to first shift the coordinate system to the point where it is being drawn.

Try adding this line to your sketch:

```java
translate(x,y);
```

See [solution](solutions/sketch_040_sinus.pde)

Let's assume we want other things to be drawn inside the sketch. For example, we could visualize the outcome of the sinus function over time. We could use the variable y and the variable angle to draw points on the horizontal axis. If we add the **point()** function to the sketch, we'll quickly notice that the point is also being affected by the rotate and translate functions we apply to the circle.

```java
stroke(0);
strokeWeight(3);
point(angle,y);
```

We need to look back into the [**pushMatrix()**](https://processing.org/reference/pushMatrix_.html) and [**popMatrix()**](https://processing.org/reference/popMatrix_.html). Used together, they allow us to to manipulate the position of the coordinate system and restore it back to its original state.

```java
pushMatrix();
stroke(0);
strokeWeight(3);
point(angle,y);
popMatrix();
```

You might have noticed that the stroke() colour settings are being now applied all other shapes. There's also pushStyle and popStyle for temporarily modify drawing settings.

```java
 pushMatrix();
 pushStyle();
 stroke(0);
 strokeWeight(3);
 point(angle, y);
 popStyle();
 popMatrix();
```

We also need to wrap pushMatrix and popMatrix around the rect function. Try it for yourself!

See [solution](solutions/sketch_041_sinus.pde)

# Colours

## A short excursion into colour theory

[__Farbenlehre__](https://en.wikipedia.org/wiki/Theory_of_Colours) (published in 1810), or __Theory of colours__ in English, is an influential book by Wolfgang von Goethe in which he describes views on the nature of colours and how these are perceived by humans. It is a more "human-centered" analyse of the perception of colour and was in some respect contrary to Isaac Newton's theories and his experiments with prisms and light. Newton, for example discovered that you could recombine all colours from the spectrum to reunite them into white light. Goethe described how the three primary colours can mix most visible colours including black. If you combine paints of different colours together, they will eventually turn into black paint because of the subtraction that happens with waves of light.

![Farbenkreis](https://bytebucket.org/frescogamba/kickstartprog/raw/f55c0a40b64f697492860b47a50630a9015c5343/misc/goethekreis.jpeg)

There are many more abstractions that show the relationship between primary colours, secondary colours, tertiary colours. Mostly, it is an illustrative organization of colour hues around a circle, also known as colour wheel.

Processing uses by default the RGB colour model. If we write for example:

```java
fill(255,0,0);
```

we define a red colour.

RGB (abbr. for red, green, and blue) is a colour model with three dimensions. You can mix the colours to produce a specific color. It's substantially different than the CMYK colour model.

![RGBCMYK](https://bytebucket.org/frescogamba/kickstartprog/raw/f55c0a40b64f697492860b47a50630a9015c5343/misc/colours_rgbcmyk.jpg)

In RGB, if we want a yellow fill, we have to mix 100% red with 100% green:

```java
fill(255,255,0);
```

Defining colours in these dimensions isn't always very intuitive except for basic colours. Can you guess what the colour of brown, darkpurple or orange is?

In Processing we can choose another colouring model, called HSB (Hue, Saturation, brightness). It is a cylindrical colour model that remaps the RGB primary colours into dimensions that are easier for us to understand.

  - Hue: it comprises a full circle of 360 degrees. The value indicates the angle on the colour wheel.
  0 rotation is red, 120 degrees green and 240 degrees blue.
  - Saturation: Between 0-100% you can control the intensity of the colour. Choose 100% for full saturation while 0% will produce something grey/white.
  - Brightness: Between 0-100% you can control the brightness of a colour. 0% brightness will produce a black colour, while a 100% brightness means that no black colour is mixed into your colour.

![HSV](https://bytebucket.org/frescogamba/kickstartprog/raw/9c45ec3235b7326eb581e00f5a28daa55f147273/misc/colours_hsv.png)

We can easily visualize the colour mode in HSV by using a for loop to draw a line with each colour. First, we need to set the colorMode with a function that has the name **colorMode()**. Within the parenthesis you'll find four parameters; the first indicating the colorMode we want to use. The other parameters are the maximum values for each of the properties, i. e. Hue, Saturation, Brightness.

```java
void setup() {
  size(360, 100);
}
void draw() {
  noStroke();
  colorMode(HSB, 360, 100, 100);
  for (int i = 0; i <= 360; i++) {
    stroke(i, 100, 100);
    line(i, 0, i, height);
  }
}
```

We are calling the stroke function inside the for loop (while i is incremented from 0 to 360). The loop changes the colour of the stroke every-time it is being called. The value for saturation and brightness are always at their maximum, at 100.


```java
stroke(0, 100, 100);
stroke(1, 100, 100);
stroke(2, 100, 100);
stroke(..., 100, 100);
stroke(359, 100, 100);
stroke(360, 100, 100);
```
Let's imagine that instead of drawing a line, we would like to create points. We replace the line function with the point() function that takes 2 parameters (x,y).

```java
void draw() {
  noStroke();
  colorMode(HSB, 360, 100, 100);
  for (int i = 0; i < 360; i++) {
    stroke(i, 100, 100);
    point(i,0);
  }
}
```

Let's assume we would like to draw points across the vertical axis and increment the hue. We'd need a second loop function:

```java
void draw() {
  noStroke();
  colorMode(HSB, 360, 100, 100);
  for (int i = 0; i < 360; i++) {
    stroke(i, 100, 100);
    point(i,0);
  }
  for (int j = 0; j < 100; j++) {
    stroke(0, j, 100);
    point(0,j);
  }
}
```

The problem is that we are calling the point function two times separately and that the second line is drawn only once. If we encapsulate the second loop inside the first one we can actually make use of both variables **i** and **j** at the same time.

```java
void draw() {
  noStroke();
  colorMode(HSB, 360, 100, 100);
  for (int i = 0; i < 360; i++) {
    for (int j = 0; j < 100; j++) {
      stroke(i, j, 100);
      point(i, 0);
    }
  }
}
```

Try this and see what happens. Does it draw points all over the screen or just on one line? Take a closer look at the point() function.

See [solution](solutions/sketch_038_colormode.pde)

 The colour theory is an almost infinitely complex and fascinating subject. There are two books which show stunning artistic experiments and visual research which I'd like to recommend: [The Art of Color](https://www.amazon.com/Art-Color-Subjective-Experience-Objective/dp/0471289280) by Johannes Itten and [Interaction of Color](https://yalebooks.yale.edu/book/9780300179354/interaction-color) by Josef Albers.
