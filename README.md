# README #

Teaching material for Kickstarting Creative Coding Workshop

### Documentation and examples ###

  * [Chapter 1 - Intro](001_intro.md)
  * [Chapter 2 - Variables](002_variables.md)
  * [Chapter 3 - Loops, patterns and repetition](003_repetition.md)
  * [Chapter 4 - Conditions](004_conditions.md)
  * [Chapter 5 + 6 - Functions & Transform](005_transform.md)
  * [Chapter 7 - Motion](007_motion.md)
  * [Chapter 8 - Mapping Math](008_mapping.md)
  * [Chapter 9 - Object Oriented Programming](009_OOP.md)
  * [Chapter 10 + 11 - External libraries](010_libraries.md)
  * [Chapter 12 + 13 - p5.js](011_p5js.md)

### Books ###

  * Ahn, Yeohyun, and Viviana Cordova. 2008. Type + Code Processing for Designers. Maryland: Center for design thinking, Maryland Institute College of Art. [Online Link](https://issuu.com/jpagecorrigan/docs/type-code_yeohyun-ahn)
  * Greenberg, Ira. 2007. Processing: Creative Coding and Computational Art. Berkeley, CA : New York: Friends of Ed ; Distributed to the book trade worldwide by Springer-Verlag. [Amazon Link](https://www.amazon.de/Processing-Computational-Foundation-Greenberg-2007-05-31/dp/B01NCQ2VM3/ref=sr_1_1?ie=UTF8&qid=1505128844&sr=8-1&keywords=Greenberg%2C+Ira.+2007.+Processing%3A+Creative+Coding+and+Computational+Art.)
  * McCarthy, Lauren, Casey Reas, and Ben Fry. 2016. Getting Started with p5.js: Making Interactive Graphics in JavaScript and Processing. [Amazon Link](https://www.amazon.de/Getting-Started-p5-js-Interactive-JavaScript/dp/1457186772/ref=sr_1_1?s=books&ie=UTF8&qid=1505128870&sr=8-1&keywords=Getting+Started+with+p5.js%3A+Making+Interactive+Graphics+in+JavaScript)
  * Reas, Casey, and Ben Fry. 2007. Processing: A Programming Handbook for Visual Designers and Artists. Cambridge, Mass: MIT Press. [Amazon Link](https://www.amazon.de/Processing-Programming-Handbook-Designers-Hardcover/dp/B00MEYJN2G/ref=sr_1_1?s=books&ie=UTF8&qid=1505128899&sr=1-1&keywords=A+Programming+Handbook+for+Visual+Designers+and+Artists)
  * Shiffman, Daniel. 2012. The Nature of Code: Simulating Natural Systems with Processing. 1 edition. The Nature of Code. [Amazon Link](https://www.amazon.de/Nature-Code-Simulating-Processing-Shiffman/dp/B00HTJOJH0/ref=sr_1_1?s=books&ie=UTF8&qid=1505128931&sr=1-1&keywords=The+Nature+of+Code%3A+Simulating+Natural+Systems+with+Processing.)
  * Wanner, Andres. 2010. Processing: eine Einführung in die Programmierung ; Version 1.1 ; [neu: Kapitel Arduino komplett überarbeitet]. Surrey, BC: Lulu Press. [Lulu Link](http://www.lulu.com/shop/andres-wanner/processing-eine-einf%C3%BChrung-in-die-programmierung-version-11/ebook/product-17469596.html)

### Who do I talk to? ###

* Gordan Savicic
