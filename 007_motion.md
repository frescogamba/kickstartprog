# Motion

The **draw()** function in Processing is an endless loop (as long as noLoop() hasn't been called). With **frameRate()** we can adjust the speed of the sketch. Processing will try do run the sketch at 60 frames per second. Therefore, Processing will try to run the draw() function 60 times per second.

You can follow the actual __real__ framerate with the **frameRate()** command:

```java
void draw() {
  println(frameRate);
}
```

## Parallax

For creating fluid animations we were usually using a variable that increments in each execution of the draw function. By using variables of the float data type we can provide more resolution to the animation. Instead of int() datatypes we should hence use float numbers for moving shapes across the screen.

Let's start by making a simple parallax animation. Disney used this simple effect with a [multi-plane camera](https://www.youtube.com/watch?v=kN-eCBAOw60) that created an illusion of space by using 2d planes.

We can make an object move by constantly updating its position with a variable:

```java
float x = 0;
float speed = 0.3;
void draw() {
  x += speed;
  rect(x,0,100,20);
}
```

Once, the ball leaves the screen, it won't come back again. Hence, we need to detect the limits by adding an if statement:

```java
float x = 0;
float speed = 0.3;
void draw() {
  x += speed;
  rect(x,0,100,20);
  if (x > width) {
    x = -10;
  }
}
```

But instead of making it appear again from the left side, I would like to have it bounce back from the right border. How can we subtract from x? The easiest strategy is to make the variable **speed** negative. Imagine this:

```java
float x = 100;
float speed = -10;
x += speed;
println(x);
```

What do you think will come out? 90 or 110? Try it out in Processing!

Once, we understand how to change the direction, we now have to detect if the object leaves the left side of the sketch:

```java
if (x < 0) {
  // do something
}
```

We have seen that a negative speed value makes the object go from right to left (positive value from left to right). How can we invert the negative sign into a positive sign and vice versa? Multiplying it with **-1** is a simple trick:

```java
float speed = -10;

void draw() {
  speed = speed * -1;
  // shorter version as with += is
  // speed *= -1;
  println(speed);
}
```

In the above statement we see that speed will change always from positive into negative and back again.

If we combine this with our moving object example, we can easily make it bounce back from the borders. Try it yourself before looking into the solution.

See [solution](solutions/sketch_034_bounceball.pde)

Can you change the y position on the ball as well? Try to use a different speed parameter. You will also need if statements to check if the ball left the sketch area.

## Random versus noise

Randomness can be a good strategy when programming organic behaviors and distribution of elements. However, randomness is not necessarily organic. An algorithm called Perlin Noise and created by Ken Perlin, takes into account a more natural random generator. His algorithm produces a smoother sequence of random numbers. The "smoothness" can actually be adjusted.

Let's create first a random graph diagramm:

```java
float x;

void setup() {
  size(400, 400);
  frameRate(60);
}
void draw() {
  x += 1;
  line(x, 0, x, random(height / 2));
}
```

It's actually upside down. Can you invert the y axis so the graph is drawn correctly?

See [solution](solutions/sketch_035_random_vs_noise.pde)

While the arguments to the random() function specify a range of values between a minimum and a maximum, noise() does not work this way. Instead, it expects an input that increments and the output range always returns a value between 0 and 1.

Let's put the noise function inside the draw loop to see how it work:

```java
void draw() {
  println(noise(5));
}
```

In the console of Processing we see the exactly the same value being printed, one after another without changing. This happens because we are asking for the result of the noise() function at the same point in time—5—over and over. If we increment the time variable t, however, we’ll get a different result. Let's try this:

```java
float tnoise = 0;

void draw() {
  tnoise += 0.01;
  println(noise(tnoise));
}
```

Let's assume we would like to compare the output of the noise() function with the output of our random number generator, we would need to create a second line graph. I will put a translate after the first line being drawn which shifts the coordinate system on the vertical axis by half of the height, so the graph is drawn exactly below the random graph.

```java
float tnoise = 0;
float x;

void draw() {
  x += 1;
  tnoise += 0.01;
  line(x, height/2, x, height/2 - random(height / 2));
  translate(0,height/2);
  line(x, 0, x, noise(tnoise));
}
```

We don't really see anything because the noise value actually outputs only values from 0 to 1. Hence we need to multiply the output of noise().

```java
  line(x, 0, x, 100 * noise(tnoise));
```

Do you see a difference? Instead of using a generic number like 100, we can make it dependent by the sketch height. So, we could write:

```java
  line(x, 0, x, height/2 * noise(tnoise));
```

What do you notice? It's again upside-down! Can you adjust the code so the vertical axis of the 2nd graph is shown correctly?

See [solution](solutions/sketch_036_random_vs_noise.pde)

How quickly we increment tnoise has direct influence on the smoothness of the noise. If you make increment tnoise a lot over time, then we are skipping ahead and the values will be more random. Try playing around with different tnoise values and see what happens:

```java
tnoise += 0.09;
// tnoise += 0.1;
// tnoise += 0.0001;
// tnoise += 5;

/* etc... */
 ```

 If we are adding +10 every time the draw() function is being called, then the noise graph almost resembles the random graph. On the contrary, an inrementation of 0.0001 almost creates a flat line. 0.09 looks like a stock market chart!

See final [sketch](solutions/sketch_037_random_vs_noise.pde)
